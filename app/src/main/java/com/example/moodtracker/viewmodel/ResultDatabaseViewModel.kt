package com.example.moodtracker.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.moodtracker.entity.DatabaseHelper
import com.example.moodtracker.entity.Mood
import kotlinx.coroutines.launch

class ResultDatabaseViewModel: ViewModel() {

    internal fun insertMood(dbHelper: DatabaseHelper, mood: Mood) {
        viewModelScope.launch {
            try{
                dbHelper.insert(mood)
            }catch (e : Exception){

            }
        }
    }

}