package com.example.moodtracker.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.moodtracker.entity.DatabaseHelper
import com.example.moodtracker.entity.Mood
import kotlinx.coroutines.launch

class MoodTrackerViewModel(private val dbHelper: DatabaseHelper) : ViewModel() {

    private val moods = MutableLiveData<List<Mood>>()

    init {
        fetchMoods()
    }

    private fun fetchMoods(){
        viewModelScope.launch {
            try{
                val moodsFromDb = dbHelper.getMoods()
                moods.postValue(moodsFromDb)
            }
            catch (e: Exception){

            }
        }
    }

    fun getMoods() : LiveData<List<Mood>> = moods
}