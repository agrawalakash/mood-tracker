package com.example.moodtracker.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.moodtracker.*
import com.example.moodtracker.entity.DatabaseHelperImpl
import com.example.moodtracker.entity.MoodDatabaseBuilder
import com.example.moodtracker.viewmodel.MoodTrackerViewModel

class MoodTrackerFragment : Fragment() {

    private lateinit var viewModel: MoodTrackerViewModel
    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_mood_tracker, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val recyclerView = view.findViewById<RecyclerView>(R.id.mood_tracker_rv)
        recyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        viewModel = MoodTrackerViewModel(
                DatabaseHelperImpl(
                        MoodDatabaseBuilder.getInstance(
                                view.context
                        )
                )
        )
        val moodsLiveData = viewModel.getMoods()
        moodsLiveData.observe(viewLifecycleOwner){
            moodList -> recyclerView.adapter = MoodTrackerAdapter(moodList)
        }
    }
}