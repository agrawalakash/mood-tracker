package com.example.moodtracker.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.moodtracker.*
import com.example.moodtracker.entity.DatabaseHelperImpl
import com.example.moodtracker.entity.Mood
import com.example.moodtracker.entity.MoodDatabaseBuilder
import com.example.moodtracker.viewmodel.ResultDatabaseViewModel
import java.util.Calendar.*

class ResultFragment : Fragment() {

    private lateinit var viewModel: ResultDatabaseViewModel

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this)[ResultDatabaseViewModel::class.java]
        return inflater.inflate(R.layout.fragment_result, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val resultTextView = view.findViewById<TextView>(R.id.result_tv)
        val currentMoodString = arguments?.let {
            when (ResultFragmentArgs.fromBundle(it).happinessCount) {
                0 -> activity?.getString(R.string.mood_test_result_zero)
                1 -> activity?.getString(R.string.mood_test_result_one)
                2 -> activity?.getString(R.string.mood_test_result_two)
                3 -> activity?.getString(R.string.mood_test_result_three)
                4 -> activity?.getString(R.string.mood_test_result_four)
                5 -> activity?.getString(R.string.mood_test_result_five)
                else -> "Cant find the correct value"
            }
        }
        currentMoodString?.let {
            resultTextView.text = it
        }

        val recordMoodButton = view.findViewById<Button>(R.id.record_mood_button)
        recordMoodButton.setOnClickListener {
            currentMoodString?.let { currentMood ->
                val calendar = getInstance()
                val date = calendar.get(DAY_OF_MONTH)
                        .toString() + "/" + (calendar.get(MONTH) + 1) + "/" + calendar.get(YEAR)

                val dbHelper = DatabaseHelperImpl(MoodDatabaseBuilder.getInstance(view.context))
                viewModel.insertMood(dbHelper, Mood(date, currentMood))
        }
    }
}

}