package com.example.moodtracker.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.moodtracker.R
import com.example.moodtracker.entity.Mood

class MoodTrackerAdapter(val moodList: List<Mood>) : RecyclerView.Adapter<MoodTrackerAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.mood_rv_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setData(moodList[position])
    }

    override fun getItemCount(): Int {
        return moodList.size
    }


    inner class ViewHolder(private val view: View): RecyclerView.ViewHolder(view){

        private val dateTextView by lazy {
            view.findViewById<TextView>(R.id.date_tv)
        }

        private val moodTextView by lazy {
            view.findViewById<TextView>(R.id.mood_tv)
        }

        fun setData(mood: Mood){
            dateTextView.text = mood.date
            moodTextView.text = mood.mood
        }
    }

}
