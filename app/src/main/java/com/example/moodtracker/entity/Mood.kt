package com.example.moodtracker.entity

import android.content.Context
import androidx.room.*

@Entity(tableName = "mood")
data class Mood(
        val date: String,
        val mood: String,
        @PrimaryKey(autoGenerate = true) val id: Int = 0
)

@Dao
interface MoodDao {

    @Insert
    suspend fun insert(mood: Mood)

    @Update
    suspend fun update(mood: Mood)

    @Delete
    suspend fun delete(mood: Mood)

    @Query("Select * from mood")
    suspend fun getAllMoods(): List<Mood>
}

@Database(entities = [Mood::class], version = 1)
abstract class MoodDatabase : RoomDatabase() {

    abstract fun moodDao(): MoodDao
}

object MoodDatabaseBuilder {

    private var INSTANCE: MoodDatabase? = null

    fun getInstance(
            context: Context
    ): MoodDatabase {
        if (INSTANCE == null) {
            synchronized(MoodDatabase::class) {
                INSTANCE = buildRoomDB(context)
            }
        }
        return INSTANCE!!
    }

    private fun buildRoomDB(context: Context) =
            Room.databaseBuilder(
                    context.applicationContext,
                    MoodDatabase::class.java,
                    "mood-tracker-database"
            ).build()
}

interface DatabaseHelper{
    suspend fun getMoods(): List<Mood>
    suspend fun insert(mood: Mood)
}

class DatabaseHelperImpl(private val moodDatabase: MoodDatabase): DatabaseHelper {
    override suspend fun getMoods(): List<Mood> {
        return moodDatabase.moodDao().getAllMoods()
    }

    override suspend fun insert(mood: Mood) {
        moodDatabase.moodDao().insert(mood)
    }

}


